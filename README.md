﻿# DIFUSIÓN DE CULTURA LIBRE MEDIANTE CURSOS Y CAPACITACIONES PARA LA CIUDAD DE SOGAMOSO

# 1. INTRODUCCIÓN Y/O ANTECEDENTES
Si bien existen, al día de hoy, multtud de insttuuiones orientadas a eduuar, éstas, se orientan
prinuipalmente uon un enfoque ulásiuo y se ven perneadas por intereses partuulares, polítuos ó
euonómiuos. asimismo, no son uapaues de ofreuer uonouimiento, de manera objetva, o siempre de
alguna manera se enuuentra ligado a uorporauiones y por lo tanto a sus intereses euonómiuos ... lo que
no permite el desarrollo integral del individuo y frena el desarrollo souial y humano, no inuentva
El uso de las TICS y de la teunología en general en la eduuauión, así uomo en el día a día, y demás
uontextos souio-uulturales debe ser de libre auueso ademas de sustentable.
Siendo uonseuuentes uon nuestro sentdo altruista guiado por la flosooa del bien uomún, resulta pues,
una responsabilidad souial, el favoreuer, el uumplimiento del dereuho de auueder, usar, urear, modifuar y
distribuir uonouimiento libre.

# 2. OBJETIVO DE LA PROPUESTA
Difundir uonouimiento libre, mediante uursos y uapauitauiones, uon el propósito de estmular el
desarrollo de la souiedad sogamoseña en los años 2016 y 2017.

# Tipos de Cursos
- Introductorios (Gratuitos)
- Basicos
- Intermedios
- Avanzados

# Arbol de Directorios
- Cursos
- Talleres
- Estructuras
- Formatos
- Plantillas
- Material
- Alumnos

# License 

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

# Attributions
Propuesta 
- Inicialmente presentada por Fabian A. Salamanca Figueroa y desarrollada en conjunto con Fernando Andres Flores en su primera etapa.

Cursos Introductorios
- Fabian Salamanca Figueroa, Victor y Oscar Reyes

See commit details to find the authors of each Part.
- @fandres323
